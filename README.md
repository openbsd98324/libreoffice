# libreoffice

## Introduction

LibreOffice is a full-featured office productivity suite that provides a near drop-in replacement for Microsoft(R) Office.

## Release


````
Linux PIW 4.9.41-v7+ #1023 SMP Tue Aug 8 16:00:15 BST 2017 armv7l GNU/Linux
ii  libreoffice                           1:5.2.7-1+rpi1                             armhf        office productivity suite (metapackage)


Linux DEVUAN ASCII 4.9.0-11-amd64 #1 SMP Debian 4.9.189-3 (2019-09-02) x86_64 GNU/Linux
ii  libreoffice                            1:5.2.7-1+deb9u11   


Linux RPI4b
uname -a ; dpkg -l | grep libreoff | head -n 1 
Linux TV 5.10.103-v7l+ #1529 SMP Tue Mar 8 12:24:00 GMT 2022 armv7l GNU/Linux
ii  libreoffice                           1:6.1.5-3+rpi1+deb10u7+rpt1 

SID AMD64, 2022 
ii  libreoffice                          1:7.0.4-4+deb11u1                  amd6
and also *chimaera devuan !!*

````




## Package


This metapackage installs all components of libreoffice:

 * libreoffice-writer: Word processor
 * libreoffice-calc: Spreadsheet
 * libreoffice-impress: Presentation
 * libreoffice-draw: Drawing
 * libreoffice-base: Database
 * libreoffice-math: Equation editor
It also recommends additional packages (e.g. fonts) in order to match an upstream LibreOffice install as closely as possible.
You can extend the functionality of LibreOffice by installing these packages:

 * hunspell-*/myspell-*: Hunspell/Myspell dictionaries
   for use with LibreOffice
 * libreoffice-l10n-*: UI interface translation
 * libreoffice-help-*: User help
 * mythes-*: Thesauri for the use with LibreOffice
 * hyphen-*: Hyphenation patterns for LibreOffice
 * libreoffice-gtk(2|3): Gtk UI Plugin, GNOME File Picker support
 * libreoffice-gnome: GIO backend
 * unixodbc: ODBC database support
 * cups-bsd: Allows LibreOffice to detect your CUPS printer queues
   automatically
 * libsane: Use your sane-supported scanner with LibreOffice
 * libxrender1: Speed up display by using Xrender library
 * libgl1: OpenGL support
 * openclipart-libreoffice: Open Clip Art Gallery with LibreOffice index
   files
 * iceweasel | firefox | icedove | thunderbird | iceape-browser | mozilla-browser:
   Mozilla profile with Certificates needed for XML Security...
 * openjdk-6-jre | gcj-jre | java5-runtime:
   Java Runtime Environment for use with LibreOffice
 * pstoedit / imagemagick: helper tools for EPS thumbnails
 * gstreamer0.10-plugins-*: GStreamer plugins for use with LibreOffices
   media backend
 * libpaper-utils: papersize detection support via paperconf
 * bluez: Bluetooth support for Impress (slideshow remote control

